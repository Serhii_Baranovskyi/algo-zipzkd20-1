﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Console;
using System.IO;


namespace algo5_6
{

    public partial class Form1 : Form
    {


        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool FreeConsole();

        string str;

        static void Fill(ref int[] arr)
        {
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = rnd.Next(0, 100);
        }
        static void FillF(ref float[] arr)
        {
            Random rnd = new Random();

            for (int i = 0; i < arr.Length; i++)
                arr[i] = rnd.Next(-20, 300);
        }



        public class Node
        {
            public int data;
            public Node next, prev;
        }

        static Node GetNode(int data)
        {
            Node newNode = new Node();

            newNode.data = data;
            newNode.prev = newNode.next = null;
            return newNode;

        }

        static Node SortInsert(Node list, Node newNode)
        {
            Node current;

            if (list == null)
                list = newNode;

            else if (list.data >= newNode.data)
            {
                newNode.next = list;
                newNode.next.prev = newNode;
                list = newNode;
            }

            else
            {
                current = list;

                while (current.next != null && current.next.data < newNode.data)
                    current = current.next;

                newNode.next = current.next;

                if (current.next != null)
                    newNode.next.prev = newNode;

                current.next = newNode;
                newNode.prev = current;

            }
            return list;
        }

        static void Insertion(ref int[] arr)
        {
            for (int i = 1; i < arr.Length; i++)
            {
                int j;
                int t = arr[i];

                for (j = i - 1; j >= 0; j--)
                {
                    if (arr[j] < t)
                        break;
                    arr[j + 1] = arr[j];
                }

                arr[j + 1] = t;
            }
        }

        static void Selection(ref int[] arr)
        {
            for (int i = 0; i < arr.Length - 1; i++)
            {
                int min = i;

                for (int j = i + 1; j < arr.Length; j++)
                {
                    if (arr[j] < arr[min])
                        min = j;
                }
                int t = arr[i];
                arr[i] = arr[min];
                arr[min] = t;
            }
        }



        static void Shells(ref float[] arr)
        {
            {
                int j;
                int step = arr.Length / 2;
                while (step > 0)
                {
                    for (int i = 0; i < (arr.Length - step); i++)
                    {
                        j = i;
                        while ((j >= 0) && (arr[j] > arr[j + step]))
                        {
                            var temp = arr[j];
                            arr[j] = arr[j + step];
                            arr[j + step] = temp;
                            j = j - step;
                        }
                    }
                    step = step / 2;
                }
            }
        }


        static Int32 add2pyramid(Int32[] arr, Int32 i, Int32 N)
        {
            Int32 imax;
            Int32 buf;
            if ((2 * i + 2) < N)
            {
                if (arr[2 * i + 1] < arr[2 * i + 2]) imax = 2 * i + 2;
                else imax = 2 * i + 1;
            }
            else imax = 2 * i + 1;
            if (imax >= N) return i;
            if (arr[i] < arr[imax])
            {
                buf = arr[i];
                arr[i] = arr[imax];
                arr[imax] = buf;
                if (imax < N / 2) i = imax;
            }
            return i;
        }

        static void Pyramid(Int32[] arr, Int32 len)
        {
            for (Int32 i = len / 2 - 1; i >= 0; --i)
            {
                long prev_i = i;
                i = add2pyramid(arr, i, len);
                if (prev_i != i) ++i;
            }

            Int32 buf;
            for (Int32 k = len - 1; k > 0; --k)
            {
                buf = arr[0];
                arr[0] = arr[k];
                arr[k] = buf;
                Int32 i = 0, prev_i = -1;
                while (i != prev_i)
                {
                    prev_i = i;
                    i = add2pyramid(arr, i, k);
                }
            }
        }

        static int[] Counting(int[] array)
        {
            var min = array[0];
            var max = array[0];
            foreach (int element in array)
            {
                if (element > max)
                {
                    max = element;
                }
                else if (element < min)
                {
                    min = element;
                }
            }

            var correctionFactor = min != 0 ? -min : 0;
            max += correctionFactor;

            var count = new int[max + 1];
            for (var i = 0; i < array.Length; i++)
            {
                count[array[i] + correctionFactor]++;
            }

            var index = 0;
            for (var i = 0; i < count.Length; i++)
            {
                for (var j = 0; j < count[i]; j++)
                {
                    array[index] = i - correctionFactor;
                    index++;
                }
            }

            return array;
        }




        double[] timesviborarr = new double[6];
        double[] timesvstavkaarr = new double[6];
        double[] timesvstavkalist = new double[6];
        double[] timesshell = new double[6];
        double[] timepyramida = new double[6];
        double[] timescount = new double[6];
        double[] nums = new double[6] { 10, 100, 500, 1000, 2000, 5000 };

        public Form1()
        {


            InitializeComponent();
            while (true)
            {

                AllocConsole();
                int[] arr10 = new int [10];
                int[] arr100 = new int[100];
                int[] arr500 = new int[500];
                int[] arr1000 = new int[1000];
                int[] arr2000 = new int[2000];
                int[] arr5000 = new int[5000];
                float[] frr10 = new float[10];
                float[] frr100 = new float[100];
                float[] frr500 = new float[500];
                float[] frr1000 = new float[1000];
                float[] frr2000 = new float[2000];
                float[] frr5000 = new float[5000];


                Stopwatch time;

                Console.WriteLine("Сортування вибором (структура даних - масив):");

                Fill(ref arr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr10.Length; i++)
                {
                    Selection(ref arr10);
                }
                time.Stop();
                timesviborarr[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 10 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr100.Length; i++)
                {
                    Selection(ref arr100);
                }
                time.Stop();

                timesviborarr[1] = time.Elapsed.TotalSeconds;
                Console.WriteLine($"Час для масиву з 100 елеметами = {time.Elapsed.TotalSeconds} s"); 
                
                Fill(ref arr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr500.Length; i++)
                {
                    Selection(ref arr500);
                }
                time.Stop();
                timesviborarr[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 500 елеметами = {time.Elapsed.TotalSeconds} s"); 

                Fill(ref arr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr1000.Length; i++)
                {
                    Selection(ref arr1000);
                }
                time.Stop();
                timesviborarr[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 1000 елеметами = {time.Elapsed.TotalSeconds} s");
                
                Fill(ref arr2000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr2000.Length; i++)
                {
                    Selection(ref arr2000);
                }
                time.Stop();
                timesviborarr[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з 2000 елеметами = {time.Elapsed.TotalSeconds} s"); 
                


                Console.WriteLine();
                Console.WriteLine("Сортування вставками (структура даних - масив):");

                Fill(ref arr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr10.Length; i++)
                {
                    Insertion(ref arr10);
                }
                time.Stop();

                timesvstavkaarr[0] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  10 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill (ref arr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr100.Length; i++)
                {
                    Insertion(ref arr100);
                }
                time.Stop();
                timesvstavkaarr[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  100 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr500.Length; i++)
                {
                    Insertion(ref arr500);
                }
                time.Stop();
                timesvstavkaarr[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  500 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr1000.Length; i++)
                {
                    Insertion(ref arr1000);
                }
                time.Stop();
                timesvstavkaarr[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  1000 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr2000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr2000.Length; i++)
                {
                    Insertion(ref arr2000);
                }
                time.Stop();
                timesvstavkaarr[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  2000 елеметами = {time.Elapsed.TotalSeconds} s");




                Console.WriteLine();

                Console.WriteLine("Сортування вставками (структура даних - список):");

                Random rnd = new Random();
                Node list = null;

                time = Stopwatch.StartNew();
                for (int i = 0; i < 10; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-200, 10)));
                }
                time.Stop();
                timesvstavkalist[0] = time.Elapsed.TotalSeconds;
                
                Console.WriteLine($"Час для масиву з  10 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 100; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-200, 10)));
                }
                time.Stop();
                timesvstavkalist[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  100 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 500; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-200, 10)));
                }
                time.Stop();
                timesvstavkalist[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  500 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 1000; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-200, 10)));
                }
                time.Stop();
                timesvstavkalist[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  1000 елеметами = {time.Elapsed.TotalSeconds} s");

                time = Stopwatch.StartNew();
                for (int i = 0; i < 2000; i++)
                {
                    list = SortInsert(list, GetNode(rnd.Next(-200, 10)));
                }
                time.Stop();
                timesvstavkalist[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  2000 елеметами = {time.Elapsed.TotalSeconds} s");



                               
                Console.WriteLine();
                Console.WriteLine("Сортування Шелла (структура даних - масив):");

                FillF(ref frr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr10.Length; i++)
                {
                    Shells(ref frr10);
                }
                time.Stop();
                timesshell[0] = time.Elapsed.TotalSeconds;
                
                Console.WriteLine($"Час для масиву з  10 елеметами = {time.Elapsed.TotalSeconds} s");

                FillF(ref frr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr100.Length; i++)
                {
                    Shells(ref frr100);
                }
                time.Stop();
                timesshell[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  100 елеметами = {time.Elapsed.TotalSeconds} s");
                FillF(ref frr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr500.Length; i++)
                {
                    Shells(ref frr500);
                }
                time.Stop();
                timesshell[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  500 елеметами = {time.Elapsed.TotalSeconds} s");
                FillF(ref frr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr1000.Length; i++)
                {
                    Shells(ref frr1000);
                }
                time.Stop();
                timesshell[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  1000 елеметами = {time.Elapsed.TotalSeconds} s");

                FillF(ref frr2000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < frr2000.Length; i++)
                {
                    Shells(ref frr2000);
                }
                time.Stop();
                timesshell[4] = time.Elapsed.TotalSeconds;
                Console.WriteLine($"Час для масиву з  2000 елеметами = {time.Elapsed.TotalSeconds} s");


                Console.WriteLine();

                Console.WriteLine("Сортування пірамідальне (структура даних - масив):");

                Fill(ref arr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr10.Length; i++)
                {
                    Pyramid(arr10, arr10.Length);
                }
                time.Stop();
                timepyramida[0] = time.Elapsed.TotalSeconds;
                
                Console.WriteLine($"Час для масиву з  10 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr100.Length; i++)
                {
                    Pyramid(arr100, arr100.Length);
                }
                time.Stop();
                timepyramida[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  100 елеметами = {time.Elapsed.TotalSeconds} s");

                Fill(ref arr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr500.Length; i++)
                {
                    Pyramid(arr500, arr500.Length);
                }
                time.Stop();
                timepyramida[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  500 елеметами = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr1000.Length; i++)
                {
                    Pyramid(arr1000, arr1000.Length);
                }
                time.Stop();
                timepyramida[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  1000 елеметами = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr2000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr2000.Length; i++)
                {
                    Pyramid(arr2000, arr2000.Length);
                }
                time.Stop();
                timepyramida[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  2000 елеметами = {time.Elapsed.TotalSeconds} s");



                Console.WriteLine();

                Console.WriteLine("Сортування Підрахунком (структура даних - масив):");

                Fill(ref arr10);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr10.Length; i++)
                {
                    Counting(arr10);
                }
                time.Stop();
                timescount[0] = time.Elapsed.TotalSeconds;
                
                Console.WriteLine($"Час для масиву з  10 елеметами = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr100);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr100.Length; i++)
                {
                    Counting(arr100);
                }
                time.Stop();
                timescount[1] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  100 елеметами = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr500);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr500.Length; i++)
                {
                    Counting(arr500);
                }
                time.Stop();
                timescount[2] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  500 елеметами = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr1000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr1000.Length; i++)
                {
                    Counting(arr1000);
                }
                time.Stop();
                timescount[3] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  1000 елеметами = {time.Elapsed.TotalSeconds} s");
                Fill(ref arr2000);

                time = Stopwatch.StartNew();
                for (int i = 0; i < arr2000.Length; i++)
                {
                    Counting(arr2000);
                }
                time.Stop();
                timescount[4] = time.Elapsed.TotalSeconds;

                Console.WriteLine($"Час для масиву з  2000 елеметами = {time.Elapsed.TotalSeconds} s");




                System.Console.WriteLine("Input ok to see graphs from first\n");
                str = System.Console.ReadLine();
                if (str == "ok")
                {
                    break;
                }

            }
            FreeConsole();
        }


        private void button1_Click(object sender, EventArgs e)
        {


            for (int i = 0; i < 6; i++)
            {
                this.chart1.Series[i].Points.Clear();
            }


            for (int i = 0; i < 3; i++)
            {
                this.chart1.Series[0].Points.AddXY(timesviborarr[i], nums[i]);
                this.chart1.Series[1].Points.AddXY(timesvstavkaarr[i], nums[i]);
                this.chart1.Series[2].Points.AddXY(timesvstavkalist[i], nums[i]);
                this.chart1.Series[3].Points.AddXY(timesshell[i], nums[i]);
                this.chart1.Series[4].Points.AddXY(timepyramida[i], nums[i]);
                this.chart1.Series[5].Points.AddXY(timescount[i], nums[i]);
            }       
        }

    }
}




