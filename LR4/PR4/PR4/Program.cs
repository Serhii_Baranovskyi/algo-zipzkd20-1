﻿//using NPOI.SS.Formula.Functions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace double_list
{
    public class DoubleList<T>
    {
        public DoubleList(T data)
        {
            Data = data;
        }

        public T Data { set; get; }
        public DoubleList<T> Pre { set; get; }
        public DoubleList<T> Next { set; get; }
    }

    public class DoubleLinkedList<T> : IEnumerable<T>
    {
        DoubleList<T> head;
        DoubleList<T> tail;
        int count = 0;

        public void AddFirst(T data)
        {
            DoubleList<T> node = new DoubleList<T>(data);
            DoubleList<T> temp = head;
            node.Next = temp;
            head = node;

            if (count == 0)
                tail = head;
            else
                temp.Pre = node;
            count++;
        }

        public void AddLast(T data)
        {
            DoubleList<T> node = new DoubleList<T>(data);

            if (head == null)
                head = node;
            else
            {
                tail.Next = node;
                node.Pre = tail;
            }
            tail = node;
            count++;
        }

        public bool Remove(T data)
        {


            DoubleList<T> current = head;

            while (current != null)
            {
                if (current.Data.Equals(data))
                {
                    break;
                }
                current = current.Next;
            }
            if (current != null)
            {
                if (current.Next != null)
                {
                    current.Pre.Next = current.Pre;
                }
                else
                {
                    tail = current.Pre;
                }

                if (current.Pre != null)
                {
                    current.Pre.Next = current.Next;
                }
                else
                {
                    head = current.Next;
                }
                count--;
                return true;
            }
            return false;
        }

        public void Clear()
        {
            head = null;
            tail = null;
            count = 0;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)this).GetEnumerator();
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            DoubleList<T> current = head;

            while (current != null)
            {
                yield return current.Data;
                current = current.Next;
            }
        }

        public IEnumerable<T> BackEnumerator()
        {
            DoubleList<T> current = tail;

            while (current != null)
            {
                yield return current.Data;
                current = current.Pre;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.ForegroundColor = ConsoleColor.Cyan;

            int menu, t;
            bool ch = true;
            DoubleLinkedList<string> list = new DoubleLinkedList<string>();

            while (ch)
            {
                Console.Write("\n1. Створення списку\n2. Вивести список на дисплей\n" +
                    "3. Додати елемент на початок\n4. Додати елемент в кінець" +
                    "\n5. Видалити елемент\n6. Знищити список\n0. Вихід\n\n");
                Console.Write("Оберіть пункт.\nПункт № ");
                while (!(int.TryParse(Console.ReadLine(), out menu)) || menu > 6 || menu < 0)
                {
                    Console.WriteLine("Введено невірне значення");
                    Console.Write("Пункт № ");
                }
                switch (menu)
                {
                    case 1:
                        Console.Clear();
                        int n;
                        Console.Write("Кількість елементів списку: ");
                        while (!(int.TryParse(Console.ReadLine(), out n)) || n < 0)
                        {
                            Console.WriteLine("Введено невірне значення");
                            Console.Write("К-ть: ");
                        }
                        for (int i = 0; i < n; i++)
                        {
                            Console.WriteLine($"Введіть елемент з індексом {i}: ");
                            while (!(int.TryParse(Console.ReadLine(), out t)) || t < 0 || t > 9)
                            {
                                Console.WriteLine("Введено невірне значення");
                                Console.Write("Елемент: ");
                            }
                            list.AddLast(t.ToString());
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 2:
                        Console.Clear();
                        foreach (var item in list)
                        {
                            Console.WriteLine(item);
                        }
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine($"Введіть елемент для додавання на початок: ");
                        while (!(int.TryParse(Console.ReadLine(), out t)) || t < 0 || t > 9)
                        {
                            Console.WriteLine("Введено невірне значення");
                            Console.Write("Елемент: ");
                        }
                        list.AddFirst(t.ToString());
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine($"Введіть елемент для додавання в кінець: ");
                        while (!(int.TryParse(Console.ReadLine(), out t)) || t < 0 || t > 9)
                        {
                            Console.WriteLine("Введено невірне значення");
                            Console.Write("Елемент: ");
                        }
                        list.AddLast(t.ToString());
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine($"Введіть елемент для видалення: ");
                        while (!(int.TryParse(Console.ReadLine(), out t)) || t > list.Count() || t < 0)
                        {
                            Console.WriteLine("Введено невірне значення");
                            Console.Write("Елемент № ");
                        }
                        list.Remove(t.ToString());
                        Console.WriteLine("Елемент видалено");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 6:
                        Console.Clear();
                        list.Clear();
                        Console.WriteLine("Список видалено");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 0:
                        Console.Clear();
                        ch = false;
                        break;
                }
            }
        }
    }
}
