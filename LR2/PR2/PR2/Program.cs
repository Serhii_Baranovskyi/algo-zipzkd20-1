﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;
using static System.Math;

namespace Generator
{
    class Program
    {
        protected static short Checked(ulong[] array, int num, int LIMIT)
        {
            short periodicity = 0;
            for (int i = 0; i < LIMIT; i++)
                if (array[i] == (ulong)num)
                    periodicity++;
            return periodicity;
        }

        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            ulong x = 0;
            ulong a = 1103515245;
            ulong c = 12345;
            ulong k = 100;
            ulong m = (ulong)Pow(2, 31);
            Console.Write("x = ");
            x = ulong.Parse(Console.ReadLine());
            int LIMIT = 10000;
            ulong[] values = new ulong[LIMIT];
            for (int i = 1; i <= values.Length; ++i)
            {
                values[i - 1] = x * k / (m - 1);
                Console.Write($"{values[i - 1],10}");
                if (i % 10 == 0)
                {
                    Console.WriteLine();
                }
                x = (a * x + c) % m;
            }

            short[] periodicity = new short[k];

            Console.WriteLine("Частота інтервалів появи випадкових величин:\n");

            for (int i = 0; i < (int)k; i++)
            {
                periodicity[i] = Checked(values, i, LIMIT);
                Console.WriteLine($"value {i + 1} = {periodicity[i]}\n");
            }

            double[] statistic = new double[k];

            Console.WriteLine("Статистична імовірність появи випадкових величин:\n");

            for (int i = 0; i < (int)k; i++)
            {
                statistic[i] = ((double)periodicity[i] / LIMIT);
                Console.WriteLine($"Value {i + 1} = {statistic[i]}\n");
            }

            double mathExp = 0;

            for (int i = 0; i < (int)k; i++)
                mathExp += i * statistic[i];

            Console.WriteLine($"Математичне сподівання випадкових величин: {mathExp}\n");

            double dyspersion = 0;
            for (int i = 0; i < (int)k; i++)
                dyspersion += Pow((i - mathExp), 2) * statistic[i];

            Console.WriteLine($"\nДисперсія випадкових величин: {dyspersion}\n");
            Console.WriteLine($"\nСередньоквадратичне відхилення випадкових величин: {Sqrt(dyspersion)}\n");
            Console.ReadLine();
        }
    }
}
