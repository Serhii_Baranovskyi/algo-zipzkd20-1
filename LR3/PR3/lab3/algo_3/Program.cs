﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using static System.Console;

using System.Text;
using System.Threading;
using System.Globalization;
using static System.Math;


namespace algo_3
{
    class Program
    {
         //<summary>
         //Главная точка входа для приложения.
         //</summary>
        [STAThread]

        static void Main(string[] args)
        {

            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

        }
    }
}
